package uk.nhs.nhsbsa.time.util;

import java.util.Arrays;
import java.util.Objects;

public class ObjectUtils {

    private ObjectUtils() {}

    public static boolean anyNull(Object... values) {
        return Arrays.stream(values).anyMatch(Objects::isNull);
    }

    public static boolean allNull(Object... values) {
        return values.length != 0 && Arrays.stream(values).allMatch(Objects::isNull);
    }

}
