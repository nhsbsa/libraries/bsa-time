package uk.nhs.nhsbsa.time.age;

import java.time.LocalDate;
import java.time.Period;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import uk.nhs.nhsbsa.time.compositedate.CompositeDate;

public class CompositeDateAgeConstraintValidator
        implements ConstraintValidator<Age, CompositeDate> {

    private DateAgeValidator delegate;

    @Override
    public void initialize(Age age) {
        delegate = new DateAgeValidator(age.comparison(), Period.parse(age.threshold()));
    }

    @Override
    public boolean isValid(CompositeDate value, ConstraintValidatorContext context) {
        boolean valid = true;
        if (value != null) {
            LocalDate localDate = value.toLocalDate();
            if (localDate != null) {
                valid = delegate.isValid(localDate);
            }
        }
        return valid;
    }
}
