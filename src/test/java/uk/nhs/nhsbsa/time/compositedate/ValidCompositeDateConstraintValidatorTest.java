package uk.nhs.nhsbsa.time.compositedate;

import static org.assertj.core.api.Assertions.assertThat;
import static uk.nhs.nhsbsa.time.test.ConstraintValidator.isValid;
import org.junit.Before;
import org.junit.Test;
import uk.nhs.nhsbsa.time.compositedate.CompositeDate;
import uk.nhs.nhsbsa.time.compositedate.ValidDate;

public class ValidCompositeDateConstraintValidatorTest {

    private CompositeDateFixture fixture;

    @Before
    public void setup() {
        // setup with sunny day data
        CompositeDate date = new CompositeDate(2000, 1, 1);
        fixture = new CompositeDateFixture();
        fixture.setRequiredDate(date);
    }

    @Test
    public void isValidWhenFieldNull() {
        fixture.setRequiredDate(null);
        assertThat(isValid(fixture)).isTrue();
    }

    @Test
    public void isValidWhenFullDate() {
        assertThat(isValid(fixture)).isTrue();
    }

    @Test
    public void requiredIsInvalidWhenFieldHasNulls() {
        fixture.setRequiredDate(new CompositeDate());
        assertThat(isValid(fixture)).isFalse();
    }

    @Test
    public void requiredIsInvalidWhenFieldBlank() {
        fixture.setRequiredDate(new CompositeDate("", "", ""));
        assertThat(isValid(fixture)).isFalse();
    }

    @Test
    public void optionalIsValidWhenFieldEmpty() {
        fixture.setOptionalDate(new CompositeDate());
        assertThat(isValid(fixture, "optionalDate")).isTrue();
    }

    @Test
    public void isInvalidWhenNullYear() {
        fixture.getRequiredDate().setYear(null);
        assertThat(isValid(fixture)).isFalse();
    }

    @Test
    public void isInvalidWhenNullMonth() {
        fixture.getRequiredDate().setMonth(null);
        assertThat(isValid(fixture)).isFalse();
    }

    @Test
    public void isInvalidWhenNullDay() {
        fixture.getRequiredDate().setDay(null);
        assertThat(isValid(fixture)).isFalse();
    }

    @Test
    public void isInvalidWhen31April() {
        fixture.setRequiredDate(new CompositeDate(2000, 4, 31));
        assertThat(isValid(fixture)).isFalse();
    }

    @Test
    public void isInvalidWhen29FebruaryInNonLeapYear() {
        fixture.setRequiredDate(new CompositeDate(2001, 2, 29));
        assertThat(isValid(fixture)).isFalse();
    }

    @Test
    public void isValidWhenZeroPaddedDayMonth() {
        fixture.setRequiredDate(new CompositeDate("2000", "04", "01"));
        assertThat(isValid(fixture)).isTrue();
    }

    @Test
    public void isInvalidWhenZeroPaddedDayGT2Chars() {
        fixture.setRequiredDate(new CompositeDate("2000", "4", "001"));
        assertThat(isValid(fixture)).isFalse();
    }

    @Test
    public void isInvalidWhenZeroPaddedMonthGT2Chars() {
        fixture.setRequiredDate(new CompositeDate("2000", "004", "01"));
        assertThat(isValid(fixture)).isFalse();
    }

    @Test
    public void isInvalidWhenZeroPaddedYearGT4Chars() {
        fixture.setRequiredDate(new CompositeDate("02000", "04", "01"));
        assertThat(isValid(fixture)).isFalse();
    }


    /**
     * Fixture class for testing.
     * 
     * @author pattu
     *
     */
    public class CompositeDateFixture {

        @ValidDate(notBlank = true)
        private CompositeDate requiredDate;

        @ValidDate
        private CompositeDate optionalDate;

        public CompositeDate getRequiredDate() {
            return requiredDate;
        }

        public void setRequiredDate(CompositeDate date) {
            this.requiredDate = date;
        }

        public CompositeDate getOptionalDate() {
            return optionalDate;
        }

        public void setOptionalDate(CompositeDate optionalDate) {
            this.optionalDate = optionalDate;
        }

        @Override
        public String toString() {
            return "CompositeDateFixture [requiredDate=" + requiredDate + ", optionalDate="
                    + optionalDate + "]";
        }
    }

}
