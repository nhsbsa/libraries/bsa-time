package uk.nhs.nhsbsa.time.util;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import uk.nhs.nhsbsa.time.util.StringUtils;

public class StringUtilsTest {

    @Test
    public void isEmptyReturnsTrueWhenNull() {
        assertThat(StringUtils.isBlank(null)).isTrue();
    }

    @Test
    public void isEmptyReturnsTrueWhenEmpty() {
        assertThat(StringUtils.isBlank("")).isTrue();
    }

    @Test
    public void isEmptyReturnsTrueWhenWhitespace() {
        assertThat(StringUtils.isBlank(" ")).isTrue();
    }

    @Test
    public void isEmptyReturnsFalseWhenNotEmpty() {
        assertThat(StringUtils.isBlank("a")).isFalse();
    }

    @Test
    public void anyEmptyReturnsTrueWhenNoArgs() {
        assertThat(StringUtils.anyBlank()).isFalse();
    }

    @Test
    public void anyEmptyReturnsTrueWhenMixedEmptys() {
        assertThat(StringUtils.anyBlank("one", null)).isTrue();
    }

    @Test
    public void anyEmptyReturnsFalseWhenNotEmpty() {
        assertThat(StringUtils.anyBlank("one")).isFalse();
    }

    @Test
    public void allEmptyReturnsFalseWhenNoArgs() {
        assertThat(StringUtils.allBlank()).isFalse();
    }

    @Test
    public void allEmptyReturnsFalseWhenMixedEmptys() {
        assertThat(StringUtils.allBlank("one", null)).isFalse();
    }

    @Test
    public void allEmptyReturnsTrueWhenEmptys() {
        assertThat(StringUtils.allBlank(null, null)).isTrue();
    }

    @Test
    public void allEmptyReturnsFalseWhenNotEmpty() {
        assertThat(StringUtils.allBlank("one")).isFalse();
    }
}
